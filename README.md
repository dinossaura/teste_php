### Instruções de configuração do projeto ###

### Como executar o projeto ###

* Com o projeto na máquina execute o comando composer install no terminal
* Execute o comando php artisan serve para subir o servidor
* No arquivo .env configure o banco de dados com seu usuário e senha, exemplo:

    * DB_CONNECTION=mysql
    * DB_HOST=127.0.0.1
    * DB_PORT=3306
    * DB_DATABASE=db_test
    * DB_USERNAME=root
    * DB_PASSWORD=root 

* Crie o banco de dados 
* Execute o comando php artisan migrate para criar as tabelas no seu banco

### Configuração de email ###

Segue um exemplo de como configrar no .env o email do qual as mensagens do
conctato serão enviadas:

   * MAIL_DRIVER=smtp
   * MAIL_HOST=smtp.gmail.com
   * MAIL_PORT=587
   * MAIL_USERNAME=seuemail@gmail.com
   * MAIL_PASSWORD=password
   * MAIL_ENCRYPTION=tls
   * MAIL_FROM_NAME="${APP_NAME}"

### Executar teste de cadastro do contato ###

* Para executar o teste de cadastro do contato execute no terminal o comando vendor/bin/phpunit

