<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Helpers\UserIpHelper;
use App\Http\Requests\ContactRequest;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index()
    {
        return view('contact');
    }

    public function store(ContactRequest $request)
    {

        $data = $request->all();

        $data['telephone'] = str_replace( array( '\'', '"', 
        ',' , ';', '<', '>', '-', '(', ')', ' '), '', $data['telephone']); 

        $data['user_ip'] =  UserIpHelper::get_ip();

        $data['file_path'] = $data['file_path']->store('files', 'public');

        $contact = new Contact();

        $createdContact = $contact->create($data);

        session()->flash('message', 'Enviado com sucesso.');

        Mail::to(env('MAIL_USERNAME'))->send(new ContactMail($createdContact));

        return redirect()->route('contact.index');

   }
}
