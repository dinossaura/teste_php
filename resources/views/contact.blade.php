@extends('layouts.front')

@section('content')

    <div class="container">
        <h1 class="title">Formulário de Contato</h1>
        <div class="wrapper animated bounceInLeft">
            <div class="contact">
                @if (session()->has('message'))
                    <div class="alert"> {{ session()->get('message') }}</div>
                @endif
                <form id="contactForm" action="{{ route('contact.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <p class="name-field">
                        <label>Nome <span>*</span></label>
                        <input type="text" name="name" id="name" required value="{{ old('name') }}">
                            @error('name')
                                <label class="error-contact">
                                    {{ $message }}
                                </label>
                            @enderror
                    </p>
                    <p class="email-field">
                        <label>Email <span>*</span></label>
                        <input type="email" name="email" id="email" required value="{{ old('email') }}">
                            @error('email')
                                <label class="error-contact">
                                    {{ $message }}
                                </label>
                            @enderror
                    </p>
                    <p class="phone-field">
                        <label>Telefone <span>*</span></label>
                        <input type="text" name="telephone" id="telephone" required value="{{ old('telephone') }}">
                            @error('telephone')
                                <label class="error-contact">
                                    {{ $message }}
                                </label>
                            @enderror
                    </p>
                    <p class="message-field full">
                        <label>Messagem <span>*</span></label>
                        <textarea name="message" rows="5" id="message" required ">{{ old('message') }}</textarea>
                            @error('message')
                                <label class=" error-contact">
                                {   { $message }}
                                </label>
                            @enderror
                    </p>
                    <p class="file-field full">
                        <label>Selecione um arquivo (pdf, doc, docx, odt ou txt) <span>*</span></label>
                        <input type="file" name="file_path" id="file" multiple="multiple" required>
                            @error('file_path')
                                <label class="error-contact">
                                    {{ $message }}
                                </label>
                            @enderror
                    </p>
                    <p class="required-field">Campo Obrigatório<span>*</span></p>
                    <p class="submit-button">
                        <button type="submit">Enviar</button>
                    </p>
                </form>
            </div>
        </div>
    </div>

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#telephone').mask('(00) 00000-0000');
        });

    </script>
@endsection

@endsection
