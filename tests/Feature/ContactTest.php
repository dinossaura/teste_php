<?php

namespace Tests\Feature;

use App\Contact;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactTest extends TestCase
{

    public function testCreateContact()
    {

        $data = [
            'name' => 'Nome Teste',
            'email' => 'nomecontato@email.com',
            'message' => 'Esta é uma menssagem teste.',
            'user_ip' => '127.0.0.1',
            'file_path' => 'files/Cexl4uyZMfKU85pZcHEQ5x4ZRhNv2wix20iqxDzs.pdf',
            'telephone' => '1146665566'
        ];

        Contact::create($data);

        $this->assertDatabaseHas('contacts', $data);

    }
}
