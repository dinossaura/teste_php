<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name' => 'required|min:3',
            'email' => 'required|min:11',
            'message' => 'required|max:255',
            'telephone' => 'required',
            'file_path' => 'required|max:500|mimes:pdf,doc,docx,odt,txt',
        ];
    }

    public function messages()
    {
        return [

            'file_path.max' => 'Arquivo deve conter no máximo 500kb.',
            'message.max' => 'É aceito apenas :max caracteres.',
            'min' => 'Campo deve ter no mínimo :min caracteres.',
            'required' => 'Este campo é obrigatório.',
            'mimes' => 'Só é possível salvar arquivos do tipo pdf, doc, docx, odt ou txt.'
            
        ];
    }
}
