<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Marketplace L6</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <style>
        @import url('https://fonts.googleapis.com/css?family=Mukta+Mahee:200,400');

        body {
            background: #52c1f7;
        }

        * {
            box-sizing: border-box;
        }

        .container {
            max-width: 700px;
            margin-left: auto;
            margin-right: auto;
            padding: 1em;
        }

        ul {
            list-style: none;
            padding: 0;
        }

        .title {
            margin-bottom: 30px;
            font-family: 'Mukta Mahee', sans-serif;
            font-weight: 400;
            font-size: 40px;
            color: #fff;
            text-align: center;
        }

        .wrapper {
            background: #fff;
            border-radius: 5px;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, .1);
            padding: 27.5px;
        }

        #contactForm {
            display: grid;
            grid-gap: 20px;
            grid-template-areas:
                'name'
                'email'
                'company'
                'telephone'
                'message';
        }

        .required-field {
            grid-column: 1 / 2;
        }

        .submit-button {
            grid-column: 2 / 3;
        }

        .name-field {
            grid-area: name;
            grid-column: 1 / 3;
        }

        .email-field {
            grid-area: email;
            grid-column: 1 / 3;
        }

        .company-field {
            grid-area: company;
            grid-column: 1 / 3;
        }

        .phone-field {
            grid-area: telephone;
            grid-column: 1 / 3;
        }

        .message-field {
            grid-area: message;
            grid-column: 1 / 3;
        }

        .file-field {
            grid-area: company;
            grid-column: 1 / 3;
        }

        /* FORM STYLES */

        .contact-us {
            margin: 0 0 30px;
            font-family: 'Mukta Mahee', sans-serif;
            font-weight: 400;
            font-size: 14px;
            color: #5dc3f2;
        }

        .contact form {
            border: 0;
        }

        .contact form label {
            display: block;
        }

        .contact form p {
            margin: 0;
            font-family: 'Mukta Mahee', sans-serif;
            font-weight: 200;
            font-size: 16px;
            color: rgba(74, 86, 96, 1);
        }

        .error-contact {
            margin: 0;
            font-family: 'Mukta Mahee', sans-serif;
            font-weight: 500;
            font-size: 12px;
            color: rgba(207, 0, 15, 1);
        }

        .contact form input,
        .contact form textarea {
            font-family: 'Mukta Mahee', sans-serif;
            font-weight: 200;
            font-size: 14px;
            width: 100%;
            padding: .4em .8em;
            background: rgba(249, 250, 250, .5);
            border: 1px solid rgba(74, 86, 96, .1);
            border-radius: 2.5px;
            outline-color: #5dc3f2;
        }

        .contact form button {
            font-family: 'Mukta Mahee', sans-serif;
            font-weight: 400;
            font-size: 12px;
            color: #fff;
            background: #5dc3f2;
            width: 90px;
            height: 30px;
            border: 0;
            border-radius: 15px;
        }

        .submit-button {
            align-self: center;
            justify-self: end;
        }

        .contact form button:hover,
        .contact form button:focus {
            background: #49b0e7;
            color: #fff;
            outline: 0;
            transition: background-color 0.3s ease-out;
        }

        .required-field {
            font-family: 'Mukta Mahee', sans-serif;
            font-weight: 200;
            font-size: 10px;
            color: rgba(74, 86, 96, 0.75);
            align-self: center;
            justify-self: start;
        }

        span {
            color: #5dc3f2;
        }

        .alert {
            margin-bottom: 1em;
            padding: 10px;
            background: #49b0e7;
            font-family: 'Mukta Mahee', sans-serif;
            font-size: 14px;
            text-align: center;
            color: #fff;
            border-radius: 2.5px;
        }

        /* LARGE SCREENS */

        @media (min-width: 500px) {
            #contactForm {
                grid-template-columns: 1fr 1fr;
                grid-template-areas:
                    'name name'
                    'email telephone'
                    'message message'
                    'file file';
            }

            .name-field {
                grid-area: name;
                grid-column: 1 / 3;
            }

            .email-field {
                grid-area: email;
                grid-column: 1 / 2;
            }

            .company-field {
                grid-area: company;
                grid-column: 2 / 3;
            }

            .phone-field {
                grid-area: telephone;
                grid-column: 2 / 3;
            }

            .message-field {
                grid-area: message;
                grid-column: 1 / 3;
            }

            .file-field {
                grid-area: file;
                grid-column: 1 / 3;
            }
        }

    </style>
</head>

<body>
    <div class="container">
        @include('flash::message')
        @yield('content')
    </div>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
    @yield('scripts')

</body>

</html>
